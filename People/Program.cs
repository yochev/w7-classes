﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace People
{
//    Задача 2
//Да се дефинира клас, описващ човек.Всеки човек има име и възраст.
//Напишете програма, която чете N на брой хора от стандартния вход
//(първи ред – име, втори ред – възраст) и извежда имената на онези от тях,
//чиято възраст е между 18 и 64 години, сортирани по азбучен ред.

    public class Program
    {
        public static void Main(string[] args)
        {
            Person[] people = InputArray();
            PrintPeopleFrom18to64(people);
        }

        static Person[] InputArray()
        {
            Console.Write("Enter the number of people:");
            int count = int.Parse(Console.ReadLine());
            Person[] people = new Person[count];

            for (int i = 0; i < people.Length; i++)
            {
                //ОПЦИЯ 1 - Зад. 2
                ////създаване на обект в масива
                //people[i] = new Person();

                //Console.WriteLine($"Enter data for person {i+1}");
                //Console.Write("Name = ");
                //people[i].Name = Console.ReadLine();

                //Console.Write("Age = ");
                //people[i].Age = int.Parse(Console.ReadLine());

                //ОПЦИЯ 2 - Зад. 3
                Console.WriteLine($"Enter data for person {i + 1}");
                Console.Write("Name = ");
                string name = Console.ReadLine();

                Console.Write("Age = ");
                int age = int.Parse(Console.ReadLine());
                                
                people[i] = new Person(name, age);
            }

            return people;
        }

        static void PrintPeopleFrom18to64(Person[] people)
        {
            for (int i = 0; i < people.Length; i++)
            {
                if (people[i].Age >= 18 && people[i].Age <= 64)
                {
                    Console.WriteLine(people[i].Name);
                }
            }
        }
    }
}