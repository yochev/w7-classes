﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Points
{
//    Задача 1
//Да се дефинира клас, описващ точка в равнината.
//Да се напише програма, която намира разстоянието между две такива точки.

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter the coordinates of two points.");
            Console.Write("x1 = ");
            double x1 = double.Parse(Console.ReadLine());

            Console.Write("y1 = ");
            double y1 = double.Parse(Console.ReadLine());

            Point p1 = new Point(x1, y1);

            Console.Write("x2 = ");
            double x2 = double.Parse(Console.ReadLine());

            Console.Write("y2 = ");
            double y2 = double.Parse(Console.ReadLine());

            Point p2 = new Point(x2, y2);
            
            //using static method
            Console.WriteLine($"The distance between the points is: {Point.CalculateDistance(p1, p2)}");

            //using instance (non-static) method
            Console.WriteLine($"The distance between the points is: {p1.CalculateDistance(p2)}");
            Console.WriteLine($"The distance between the points is: {p2.CalculateDistance(p1)}");

        }
    }
}